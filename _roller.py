import math
from functions import inputSolidParam


def getParam(needDensity):
    radius = inputSolidParam('Radius:')
    height = inputSolidParam('Height length: ')
    if needDensity:
        density = inputSolidParam('Density: ')
        return [radius, height, density]
    return [radius, height]


def volume(param):
    radius = param[0]
    height = param[1]
    return math.pi * math.pow(radius, 2) * height


def mass(param):
    radius = param[0]
    height = param[1]
    density = param[2]
    return volume([radius, height]) * density


def area(param):
    radius = param[0]
    height = param[1]
    return 2 * math.pi * radius * (radius + height)
