import math
from functions import inputSolidParam


def getParam(needDensity):
    radius = inputSolidParam('Radius: ')
    if needDensity:
        density = inputSolidParam('Density: ')
        return [radius, density]
    return [radius]


def volume(param):
    radius = param[0]
    return (4/3) * math.pi * math.pow(radius, 3)


def mass(param):
    radius = param[0]
    density = param[1]
    return volume([radius]) * density


def area(param):
    radius = param[0]
    return 4 * math.pi * math.pow(radius, 2)
