import math
from functions import inputSolidParam


def getParam(needDensity):
    height = inputSolidParam('Height length: ')
    baseSide1 = inputSolidParam('Base side length (1): ')
    baseSide2 = inputSolidParam('Base side length (2): ')
    if needDensity:
        density = inputSolidParam('Density: ')
        return [height, baseSide1, baseSide2, density]
    return [height, baseSide1, baseSide2]


def volume(param):
    height = param[0]
    base1 = param[1]
    base2 = param[2]
    return (1/3) * (base1 * base2) * height


def mass(param):
    height = param[0]
    base1 = param[1]
    base2 = param[2]
    density = param[3]
    return volume([height, base1, base2]) * density


def area(param):
    height = param[0]
    base1 = param[1]
    base2 = param[2]
    return (base1 * base2) + (base1 * sideWallHeight(height, base2)) + (base2 * sideWallHeight(height, base1))


def sideWallHeight(mainHeight, base):
    return math.sqrt(math.pow(mainHeight, 2) + math.pow(base/2, 2))
