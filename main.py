from whaaaaat import prompt
from functions import calcSolid, finalAnswer, getSolidName
import _sphere as Sphere
import _tetrahedron as Tetrahedron
import _pyramid as Pyramid
import _cone as Cone
import _roller as Roller


def solidCalculator(*params):
    # dict of choices
    solidsChoices = {'Sphere': Sphere, 'Regular Tetrahedron': Tetrahedron, 'Simple Pyramid with a Rectangular Base':
                     Pyramid, 'Cone': Cone, 'Roller': Roller}
    actionChoices = {'Calculate Volume': False, 'Calculate Mass': True,
                     'Calculate Surface Area': False, 'Calculate Everything!': True}
    lengthChoices = {'Millimetres [mm]': 'mm', 'Centimetres [cm]': 'cm',
                     'Metres [m]': 'm', 'Kilometres [km]': 'km', 'Miles [mi]': 'mi'}
    massChoices = {'Grams [g]': 'g',
                   'Kilograms [kg]': 'kg', 'Pounds [lbs]': 'lbs'}

    # dict for final answer
    finalActions = {'Calculate Volume': 'Volume', 'Calculate Mass': 'Mass',
                    'Calculate Surface Area': 'Surface area'}
    actions = ['Calculate Volume',
               'Calculate Mass', 'Calculate Surface Area']

    if not params:
        data = [
            {
                'type': 'list',
                'name': 'solid',
                'message': 'What kind of solid you want to work with?',
                'choices': solidsChoices
            },
            {
                'type': 'list',
                'name': 'action',
                'message': 'What action do you want to perform on your solid?',
                'choices': actionChoices
            },
            {
                'type': 'list',
                'name': 'unitsLength',
                'message': 'What units of length you want to use?',
                'choices': lengthChoices
            }
        ]

        solid = prompt(data)

        # data needed to calculation
        thisSolid = solidsChoices[solid['solid']]
        thisAction = solid['action']
        thisLengthUnits = lengthChoices[solid['unitsLength']]
        units = [thisLengthUnits]

        needDensity = actionChoices[solid['action']]

        if needDensity:  # ask about mass units only if calculate mass
            extraData = [
                {
                    'type': 'list',
                    'name': 'unitsMass',
                    'message': 'What units of mass you want to use?',
                    'choices': massChoices
                }
            ]
            massUnits = prompt(extraData)

            # data needed to calculation
            thisMassUnits = massChoices[massUnits['unitsMass']]
            units.append(thisMassUnits)

        print('*********************************************')
        print('Ok! Now, give me parameters of your ' +
              str(solid['solid']) + ':')
        parameters = thisSolid.getParam(needDensity)
    else:
        thisSolid = params[0]
        thisAction = params[1]
        thisLengthUnits = params[2]
        thisMassUnits = params[3]
        parameters = params[4]
        units = [thisLengthUnits, thisMassUnits]

    if thisAction == 'Calculate Everything!':  # do every calculation
        for action in actions:
            solidCalculator(
                thisSolid, action, thisLengthUnits, thisMassUnits, parameters)
    else:  # do only one from calculation
        result = calcSolid(thisSolid, thisAction, parameters)
        actionToPrint = finalActions[thisAction]
        solidToPrint = getSolidName(
            thisSolid, solidsChoices)

        answer = finalAnswer(actionToPrint, solidToPrint, result, units)

        print('*********************************************')
        print(answer)


solidCalculator()
