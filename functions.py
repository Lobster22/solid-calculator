def inputSolidParam(message):
    correctParam = False
    while not(correctParam):
        param = input(message)
        try:
            floatParam = float(param)
        except:
            print('Parameters must be NUMBERS. LOL')
        else:
            if floatParam <= 0:
                print('Parameters must be numbers BIGGER THAN ZERO.')
            else:
                correctParam = True

    return floatParam


def calcSolid(solid, action, parameters):
    if action == 'Calculate Volume':
        return solid.volume(parameters)
    elif action == 'Calculate Mass':
        return solid.mass(parameters)
    elif action == 'Calculate Surface Area':
        return solid.area(parameters)
    else:
        return 'Something went wrong with calcSolid function.'


def finalAnswer(action, solid, result, units):
    answer = action + ' of your ' + solid + ' is: ' + str(result) + ' '
    if action == 'Mass':
        answer += units[1]
    elif action == 'Volume':
        answer += units[0] + '3'
    elif action == 'Surface area':
        answer += units[0] + '2'

    return answer


def getSolidName(solid, arrOfSolids):
    for solidName, solidObject in arrOfSolids.items():
        if solid == solidObject:
            return solidName

    return "solid name doesn't exist"
