import math
from functions import inputSolidParam


def getParam(needDensity):
    side = inputSolidParam('Side length: ')
    if needDensity:
        density = inputSolidParam('Density: ')
        return [side, density]
    return [side]


def volume(param):
    side = param[0]
    return (math.pow(side, 3) * math.sqrt(2)) / 12


def mass(param):
    side = param[0]
    density = param[1]
    return volume([side]) * density


def area(param):
    side = param[0]
    return math.pow(side, 2) * math.sqrt(3)
